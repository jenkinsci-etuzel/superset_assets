(window.webpackJsonp=window.webpackJsonp||[]).push([[34],{4546:function(e,o,t){"use strict";t.r(o),t.d(o,"default",(function(){return c}));t(95);var r=t(186),n=t.n(r),s=t(1),i=t(47),h=t.n(i),p=t(2);const l=h.a.div`
  background-color: ${({theme:e})=>e.colors.secondary.light2};
  padding: ${({theme:e})=>4*e.gridUnit}px;
  border-radius: ${({theme:e})=>2*e.gridUnit}px;
  height: ${({height:e})=>e};
  width: ${({width:e})=>e};
  overflow-y: scroll;

  h3 {
    /* You can use your props to control CSS! */
    font-size: ${({theme:e,headerFontSize:o})=>e.typography.sizes[o]};
    font-weight: ${({theme:e,boldText:o})=>e.typography.weights[o?"bold":"normal"]};
  }
`;class c extends s.PureComponent{constructor(){super(...arguments),this.rootElem=Object(s.createRef)()}componentDidMount(){const e=this.rootElem.current;console.log("Plugin element",e)}render(){console.log("Approach 1 props",this.props);const{data:e,height:o,width:t}=this.props;return console.log("Plugin props",this.props),Object(p.jsx)(l,{ref:this.rootElem,boldText:this.props.boldText,headerFontSize:this.props.headerFontSize,height:o,width:t},Object(p.jsx)("h3",null,this.props.headerText),Object(p.jsx)("pre",null,n()(e,null,2)))}}}}]);